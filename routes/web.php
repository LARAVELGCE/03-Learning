<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Para las traducciones en nuestra web
Route::get('/set_language/{lang}', 'Controller@setLanguage')->name( 'set_language');

// Rutas para el login/{recibira un driver bien facebook o github}
Route::get('login/{driver}', 'Auth\LoginController@redirectToProvider')->name('social_auth');
Route::get('login/{driver}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

// Ruta para la obtener la ruta la imagen
Route::get('/images/{path}/{attachment}', function($path, $attachment) {
    $file = sprintf('storage/%s/%s', $path, $attachment);
    // storage/%s/%s => como vemos se genera  en public/store/courses|%s|$path/imagenes|%s|$attachment
    if(File::exists($file)) {
        return Image::make($file)->response();
    }
});


// Rutas para obtener y procesar la suscripcion
Route::group(['middleware' => ['auth']], function () {
    Route::group(["prefix" => "subscriptions"], function() {
        Route::get('/plans', 'SubscriptionController@plans')
            ->name('subscriptions.plans');
        // Ruta luego de suscribirse
        Route::get('/admin', 'SubscriptionController@admin')
            ->name('subscriptions.admin');

        Route::post('/process_subscription', 'SubscriptionController@processSubscription')
            ->name('subscriptions.process_subscription');
        // Rutas para renaudar y cancelar suscripcion
        Route::post('/resume', 'SubscriptionController@resume')->name('subscriptions.resume');
        Route::post('/cancel', 'SubscriptionController@cancel')->name('subscriptions.cancel');

    });
    // Para las facturas
    Route::group(['prefix' => "invoices"], function() {
        // Para poder administrarla
        Route::get('/admin', 'InvoiceController@admin')->name('invoices.admin');
        // Para poder descargar las facturas
        Route::get('/{invoice}/download', 'InvoiceController@download')->name('invoices.download');
    });
});
/*
 * Luego ejecutas un enlace simbolico para utilizar el directorio store de la imagem
 * en terminal ejecutas la ruta
 * php artisan storage:link
 * */

// Creamos un grupo de rutas de courses y le paso un parametro a la vista
Route::group(['prefix' => 'courses'], function () {
    Route::group(['middleware' => ['auth']], function() {
        // Para obtener las suscribirse al curso que sea sacado de la tabla course_student
        Route::get('/subscribed', 'CoursesController@subscribed')->name('courses.subscribed');
        Route::get('/{course}/inscribe', 'CoursesController@inscribe')->name('courses.inscribe');
        // Para procesar las reseñas de los cursos
        Route::post('/add_review', 'CoursesController@addReview')->name('courses.add_review');
        // Usamos el middleware con una funcion sprintf (parametro1 , parametro 2)
        // (nombre del middleware [en mi caso role] luego con :$s es segundo parametro que en este caso
        // sera \App\Role::TEACHER el rol Teacher que es sacado del modelo Role) y ademas usaremos todos los metodos con resource
        // index,store,create,destroy,update,show
        Route::group(['middleware' => [sprintf('role:%s', \App\Role::TEACHER)]], function () {
          Route::resource('courses', 'CoursesController');
       });

    });
    Route::get('/{course}', 'CoursesController@show')->name('courses.detail');
});

// Ruta para gestionar a los usuarios
Route::group(["prefix" => "profile", "middleware" => ["auth"]], function() {
    Route::get('/', 'ProfileController@index')->name('profile.index');
    Route::put('/', 'ProfileController@update')->name('profile.update');
});

// Para poder hacer solicitud para sea profesor
Route::group(['prefix' => "solicitude"], function() {
    Route::post('/teacher', 'SolicitudeController@teacher')->name('solicitude.teacher');
});

//  Obtener los cursos y estydiantes que pertenecer al profesor
Route::group(['prefix' => "teacher", "middleware" => ["auth"]], function() {
    // Ruta para que liste los cursos que imparte el teacher y se lista asociando al teacher
    Route::get('/courses', 'TeacherController@courses')->name('teacher.courses');
    Route::get('/students', 'TeacherController@students')->name('teacher.students');
    // ruta para enviar el mensaje tipo post
    Route::post('/send_message_to_student', 'TeacherController@sendMessageToStudent')->name('teacher.send_message_to_student');

});

// Rutas para solo los administradores y autentificados
Route::group(['prefix' => "admin", "middleware" => ['auth', sprintf("role:%s", \App\Role::ADMIN)]], function() {
    // para mostrar el componente que vamos a generar con vue.js
    Route::get('/courses', 'AdminController@courses')->name('admin.courses');
    // obtener los datos de los cursos en formato json
    Route::get('/courses_json', 'AdminController@coursesJson')->name('admin.courses_json');
    // actualizar el estado del curso y enciar un email al profesor si se ha publicado o sea ha rechazado
    Route::post('/courses/updateStatus', 'AdminController@updateCourseStatus');

    Route::get('/students', 'AdminController@students')->name('admin.students');
    Route::get('/students_json', 'AdminController@studentsJson')->name('admin.students_json');
    Route::get('/teachers', 'AdminController@teachers')->name('admin.teachers');
    Route::get('/teachers_json', 'AdminController@teachersJson')->name('admin.teachers_json');
});
