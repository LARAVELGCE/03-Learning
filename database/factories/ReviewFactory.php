<?php

use Faker\Generator as Faker;

$factory->define(App\Review::class, function (Faker $faker) {
    return [
        'course_id' => \App\Course::all()->random()->id,
        // Numero de decimales es 2 minmo entre 1 y  5
        'rating' => $faker->randomFloat(2, 1, 5)
    ];
});
