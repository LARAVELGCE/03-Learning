<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Iniciar y eliminar para volver a ejecutar los curso
        Storage::deleteDirectory('courses');
        Storage::deleteDirectory('users');

        Storage::makeDirectory('courses');
        Storage::makeDirectory('users');

        // Como ven aqui cree el name admin, teacher y student y ya no
        // le hacemos caso   'name' => $faker->word, del RoleFactory
        // pero la descripcion sii normal se usa del factory
        factory(\App\Role::class, 1)->create(['name' => 'admin']);
        factory(\App\Role::class, 1)->create(['name' => 'teacher']);
        factory(\App\Role::class, 1)->create(['name' => 'student']);
        // Creamos un usuario con ROL admin
        factory(\App\User::class, 1)->create([
            'name' => 'admin',
            'email' => 'admin@mail.com',
            'password' => bcrypt('secret'),
            'role_id' => \App\Role::ADMIN
        ])
            // y  la vez le creamos se creo un student con ese usuario
            ->each(function (\App\User $u) {
                factory(\App\Student::class, 1)->create(['user_id' => $u->id]);
            });

        //  Creamos un usuario con ROL STUDENT
        factory(\App\User::class, 1)->create([
            'name' => 'student',
            'email' => 'student@mail.com',
            'password' => bcrypt('secret'),
            'role_id' => \App\Role::STUDENT
        ])
            // y  la vez le creamos se creo un student con ese usuario
            ->each(function (\App\User $u) {
                factory(\App\Student::class, 1)->create(['user_id' => $u->id]);
            });

        //  Creamos 50 user ahora si con datos del FAKER y cada uno sera un studiante
        factory(\App\User::class, 50)->create()
            ->each(function (\App\User $u) {
                factory(\App\Student::class, 1)->create(['user_id' => $u->id]);
            });

        //  Creamos 10 user ahora si con datos del FAKER la cual sera relacionado tanto Student como  Teacher
        factory(\App\User::class, 10)->create()
            ->each(function (\App\User $u) {
                factory(\App\Student::class, 1)->create(['user_id' => $u->id]);
                factory(\App\Teacher::class, 1)->create(['user_id' => $u->id]);
            });
        // Crear 3 Niveles por nosotros mismo sin depender del faker
        factory(\App\Level::class, 1)->create(['name' => 'Beginner']);
        factory(\App\Level::class, 1)->create(['name' => 'Intermediate']);
        factory(\App\Level::class, 1)->create(['name' => 'Advanced']);
        // Crear 5  Category
        factory(\App\Category::class, 5)->create();

        // Crear 50 cursos
        factory(\App\Course::class, 50)
            ->create()
            ->each(function (\App\Course $c) {
                // Por cada curso crea 2 metas y 4 requerimientos
                $c->goals()->saveMany(factory(\App\Goal::class, 2)->create());
                $c->requirements()->saveMany(factory(\App\Requirement::class, 4)->create());
            });


    }
}
