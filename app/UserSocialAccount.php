<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSocialAccount extends Model
{
/*Add [user_id] to fillable property to allow mass assignment on [App\UserSocialAccount].
Agregue [user_id] a la propiedad rellenable para permitir la asignación masiva en [App \ UserSocialAccount].*/
 //   $fillable => QUE DATOS QUEREMOS INSERTAR
    protected $fillable = ['user_id', 'provider', 'provider_uid'];

    // Como mi tabla  user tiene created_at y updated_at
    //  y la tabla user_social_account no lo tiene por eso busca
    // y no lo tiene para eso vamos a deshabilitarlo

/*SQLSTATE[42S22]: Column not found: 1054 Unknown column 'updated_at' in 'field list'
(SQL: insert into `user_social_accounts` (`user_id`, `provider`, `provider_uid`, `updated_at`, `created_at`)
values (64, github, 17607441, 2018-04-25 00:16:26, 2018-04-25 00:16:26))*/
    public $timestamps = false;

    // Una red social pertenece a un usuario
    public function user () {
        return $this->belongsTo(User::class);
    }
}
