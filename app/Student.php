<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Student
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property string|null $title
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Student whereUserId($value)
 */
class Student extends Model
{

/*Add [user_id] to fillable property to allow mass assignment on [App\Student].
Agregue [user_id] a la propiedad de relleno para permitir la asignación masiva en [App \ Student].*/
    protected $fillable = ['user_id', 'title'];

    // courses_formatted => para que el curso sea formateado
    // en la linea  46
    protected $appends = ['courses_formatted'];

    // Un estudiante puede tener muchos curso
    public function courses()
    {
        return $this->belongsToMany(Course::class);
    }
    // Un estudiante pertenece a un usuario pasandole el id del user el role_id y name
    public function user()
    {
        return $this->belongsTo(User::class)->select('id', 'role_id', 'name', 'email');
    }

    // con get attribute
    // con pluck para mostrar las columnas que tenemos
    public function getCoursesFormattedAttribute () {
        // con implode mostrara el nombre de los cursos con un salto
        // de linea
        return $this->courses->pluck('name')->implode('<br />');
        // esto devolver con salto de linea
        // curso 1
        // curso 2
        // curso 3
    }
}