<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 * @property int $id
 * @property int $role_id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $stripe_id
 * @property string|null $card_brand
 * @property string|null $card_last_four
 * @property string|null $trial_ends_at
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCardBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCardLastFour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereStripeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTrialEndsAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
class User extends Authenticatable
{
    // Usaremos el Billable para suscripciones esto viene
    // de use Laravel\Cashier\Billable;
    use Notifiable, Billable;

    // Un evento cuando el usuario se esta creando
    protected static function boot () {
        parent::boot();
        static::creating(function (User $user) {
            // si esto no se ejecuta  por terminal osea con tinker
            // el slug sera suu nombre seguido de su apellido
            if( ! \App::runningInConsole()) {
                $user->slug = str_slug($user->name . " " . $user->last_name, "-");
            }
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
        // si el usuario esta autenficado devolvera el nombre del rol
        // si no esta esta autenficado devolvera usuario invitado
    public static function navigation () {
        return auth()->check() ? auth()->user()->role->name : 'guest';
    }
    // Funcion para obtener la imagen
    public function pathAttachment () {
        return "/images/users/" . $this->picture;
    }

    // Un usuario pertenece a un rol
    public function role () {
        return $this->belongsTo(Role::class);
    }
    // Un usuario pertenece solo a  un  estudiante
    public function student () {
        return $this->hasOne(Student::class);
    }
    // Un usuario pertenece solo a  un  profesor
    public function teacher () {
        return $this->hasOne(Teacher::class);
    }
    // Un usuario pertenece a solo a red social
    public function socialAccount () {
        return $this->hasOne(UserSocialAccount::class);
    }
}
