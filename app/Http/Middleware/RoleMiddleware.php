<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        // creamos este middleware para que los usuarios que no
        // sean teacher no puedan
        //          crear curso
        //          cursos desarrollados por mi
        // con int lo parseas el rol a numero entero
        if ( auth()->user()->role_id !== (int) $role) {
            // abortamos con un codigo 401 que significa no puede acceder
            // y le mandamos este mensaje
            abort(401, __("No puedes acceder a esta zona"));
        }
        return $next($request);
    }
}
