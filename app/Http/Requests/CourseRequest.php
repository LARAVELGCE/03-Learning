<?php

namespace App\Http\Requests;

use App\Role;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;



class CourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // solo lo que estan autentificados con el rol de teacher pueden acceder a las validaciones
        //
        return auth()->user()->role_id === Role::TEACHER;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            // No hay validaciones ni para get ni para delete
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST': {
                return [
                    'name' => 'required|min:5',
                    'description' => 'required|min:30',
                    // para el level_id una regla es tambien que el mismo id debe existir en la tabla levels
                    'level_id' => [
                        'required',
                        Rule::exists('levels', 'id')
                    ],
                    // para el category_id una regla es tambien que el mismo id debe existir en la tabla categories
                    'category_id' => [
                        'required',
                        Rule::exists('categories', 'id')
                    ],
                    'picture' => 'required|image|mimes:jpg,jpeg,png',
                    // para que el primer requerimiento se rellene necesitara del requerimiento 2 este completado
                    'requirements.0' => 'required_with:requirements.1',
                    'goals.0' => 'required_with:goals.1',
                ];
            }
            // para actualizar
            case 'PUT': {
                return [
                    'name' => 'required|min:5',
                    'description' => 'required|min:30',
                    // para el level_id una regla es tambien que el mismo id debe existir en la tabla levels
                    'level_id' => [
                        'required',
                        Rule::exists('levels', 'id')
                    ],
                    // para el category_id una regla es tambien que el mismo id debe existir en la tabla categories
                    'category_id' => [
                        'required',
                        Rule::exists('categories', 'id')
                    ],
//                    sometime hara que se cumpla que sea imagen y qye sea jpg png etc
                    'picture' => 'sometimes|image|mimes:jpg,jpeg,png',
                    // para que el primer requerimiento se rellene necesitara del requerimiento 2 este completado
                    'requirements.0' => 'required_with:requirements.1',
                    'goals.0' => 'required_with:goals.1',
                ];
            }
        }
    }
}
