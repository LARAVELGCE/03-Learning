<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
	public function __construct() {
		$this->middleware(function($request, $next) {
			if ( auth()->user()->subscribed('main') ) {
				return redirect('/')
					->with('message', ['warning', __("Actualmente ya estás suscrito a otro plan")]);
			}
			return $next($request);
		})
		->only(['plans', 'processSubscription']);
	}

	public function plans () {
		return view('subscriptions.plans');
    }

    public function processSubscription () {
        $token = request('stripeToken');
        try {
            // si el usuario esta enviando un cupon
            if ( \request()->has('coupon')) {
                // creara una nueva suscripcion con el tipo que que eligio
                // y con el cupon  que mando se le creara un nuevo token
                // que va hacer procesado por stripeToken
                \request()->user()->newSubscription('main', \request('type'))
                    ->withCoupon(\request('coupon'))->create($token);
            } else {
                // esto sera si no tiene un cupon
                \request()->user()->newSubscription('main', \request('type'))
                    ->create($token);
            }

            // Luego de que se proceso la suscripcion pasamos aqui
            return redirect(route('subscriptions.admin'))
                ->with('message', ['success', __("La suscripción se ha llevado a cabo correctamente")]);
        } catch (\Exception $exception) {
            $error = $exception->getMessage();
            return back()->with('message', ['danger', $error]);
        }

    }
    public function admin () {
        $subscriptions = auth()->user()->subscriptions;
        return view('subscriptions.admin', compact('subscriptions'));
    }

    public function resume () {
       /* <input type="hidden" name="plan" value="{{ $subscription->name }}" />
       este codigo viene de view/subscriptions/admin.blade ya que de ahi obtendrremos el
       plan que esta afiliado name="plan" con el atributo name lo sacamos */
        $subscription = \request()->user()->subscription(\request('plan'));
        // si la suscripcion esta cancelada y la suscripcion esta e¡activa o periodo de gracia
        if ($subscription->cancelled() && $subscription->onGracePeriod()) {
            \request()->user()->subscription(\request('plan'))->resume();
            return back()->with('message', ['success', __("Has reanudado tu suscripción correctamente")]);
        }
        return back();
    }

    public function cancel () {
        auth()->user()->subscription(\request('plan'))->cancel();
        return back()->with('message', ['success', __("La suscripción se ha cancelado correctamente")]);
    }


}
