<?php

namespace App\Http\Controllers;

use App\Course;
use App\Mail\MessageToStudent;
use App\Student;
use App\User;
use Illuminate\Http\Request;


class TeacherController extends Controller
{
    public function courses () {
        // creo un variable $courses luego con la propiedad withCount nos servira
        // para contar a los students que estan escritos al curso
        // para luego llamarlo students_count en la vista
        // relacionandolo con la tabla category y reviews
        // y listarlo solo a los curso que solo sean del teacher
        // listarlo y paginarlo de 12 con paginate
        $courses = Course::withCount(['students'])->with('category', 'reviews')
            // con withTrashed() muestras cursos que han sido borrado de manera logica
           /* ->whereTeacherId(auth()->user()->teacher->id)->withTrashed()->paginate(12);*/
            ->whereTeacherId(auth()->user()->teacher->id)->paginate(12);
        return view('teachers.courses', compact('courses'));
    }

    public function students()
    {
        // with para obtener las relaciones en este caso el user y el courses.reviews
        $students = Student::with('user', 'courses.reviews')
            // quremos que tenga cursos
                // y seleccionamos el id del teacher
                // y seleccionaremos el teacher_id que viene de la relacion user()
                // withTrashed(); -> solo es usado para tablas que usan softdeletes
                // la cual recuperaba datos que no sean null
            ->whereHas('courses', function ($q) {
                $q->where('teacher_id', auth()->user()->teacher->id)->select('id', 'teacher_id', 'name')->withTrashed();
            })->get();

        // -> obtendremos cada una de las filas y es pasado en el datatable
        $actions = 'students.datatables.actions';
        // agregaremos una columna llamada actions y le mandamos la variable
        // $actions donde cargara el id de user
        // rawcolumns dira que la columna sera convertido a html
        // y le devolvemos con courses_formatted
        // por ultimo make(true); transforma a datatable
        return \DataTables::of($students)->addColumn('actions', $actions)->rawColumns(['actions', 'courses_formatted'])->make(true);
    }

    public function sendMessageToStudent()
    {
        // la variable que recupero  la informacion (osea el mensaje en el modal) via ajax
        $info = \request('info');
        // data sera igual a un array
        $data = [];
        // vamos a parsear a string la informacion y lo metemos a data
        parse_str($info, $data);
        // vamos a locar el id del usuario
        $user = User::findOrFail($data['user_id']);
        try {
            // le mandaremos un email al usuario
            // instanciando de MessageToStudent las dos variabkes que requiere
            // en este caso el nombre del teacher
            // y la informacion de mensaje
            \Mail::to($user)->send(new MessageToStudent( auth()->user()->name, $data['message']));
            // si todos ha ido bien $success es verdad
            $success = true;
        } catch (\Exception $exception) {
            $success = false;
        }
        // aqui nos mostrara la respuesta del res
        return response()->json(['res' => $success]);

    }
}
