<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function setLanguage ($language) {

        // Cuando enviamos el idiomoa
        // le enviamos config('languages') => en y es
        if(array_key_exists( $language, config('languages'))) {
            // con el metodo put establecemos llamada applocale
            // pasandole una variable $language
            session()->put('applocale', $language);
        }
        return back();

    }
}
