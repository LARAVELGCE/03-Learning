<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Para accceder a la vista home primero tenemos autenficarnos pero con excep
        // le vamos a dar permiso para que solo la vista index este permitida por todos
        $this->middleware('auth')->except(['index']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // withCount : coger el conteo
        $courses = Course::withCount(['students'])
            // a la vez con las relaciones que estableciomos en el modelo curso
            ->with('category', 'teacher', 'reviews')
            // todos los cursos con estado publicado
            ->where('status', Course::PUBLISHED)
            // descendente
            // ->latest() descendde por created_at
            ->oldest() //  ascendente por created_at
            ->paginate(12);

        return view('home',compact('courses'));
    }
}
