<?php

namespace App\Http\Controllers;

use App\Course;
use App\Review;
use Illuminate\Http\Request;
use App\Mail\NewStudentInCourse;

use App\Helpers\Helper;

use App\Http\Requests\CourseRequest;

class CoursesController extends Controller
{
  //   /{course} => como vemos le paso todos el parametro
    //  y lo pinto en pantalla con dd
    public function show (Course $course) {
        // con load obtengo las relaciones de las demas tablas con el curso

        $course->load([
            'category' => function ($q) {
                $q->select('id', 'name');
            },
            'goals' => function ($q) {
                $q->select('id', 'course_id', 'goal');
            },
            'level' => function ($q) {
                $q->select('id', 'name');
            },
            'requirements' => function ($q) {
                $q->select('id', 'course_id', 'requirement');
            },
            'reviews.user',
            'teacher'
        ])->get();
         $related =  $course->relatedCourses();
        return view('courses.detail', compact('course', 'related'));


//        http://dev.learning:8080/courses/qui-sit-explicabo-mollitia-nemo-quae
        // asi se mostrara en pantalla con el slug que le declare  getRouteKeyName() en course
    }

    public function inscribe (Course $course) {
        // dentro de la tabla course_student insertar el id del estudiante
        $course->students()->attach(auth()->user()->student->id);
        // Te acuerdas Mail/NewStudentInCourse en ese constructr mandas 2 parametreos pz
        // aqui vemos que se le pasara  public function __construct(Course $course, $student_name)
        // el primero esta claro con todos los parametros del curso y en $student_name se enviara
        //  auth()->user()->name del estuidante en este caso el nombre del usuario estudiante

        // to => sera enviado al teacher con los datos del curso que se inscribio es estudiante

        \Mail::to($course->teacher->user)->send(new NewStudentInCourse($course, auth()->user()->name));
        return back()->with('message', ['success', __("Inscrito correctamente al curso")]);
    }

    public function subscribed () {
        // creo una variable para que este curso tenga estudiantes
        $courses = Course::whereHas('students', function($query) {
            // ademas que me y dentro de la tabla students
            // me seleccione el user_id que sera el id de mi autentificacion
            $query->where('user_id', auth()->id());
        })->get();
        // Manda a la vista con la variable courses
        return view('courses.subscribed', compact('courses'));
    }

    public function addReview () {
        // Pasaremos un array y lo creamos con un request pasamos este formulario post
        Review::create([
            "user_id" => auth()->id(),
            "course_id" => request('course_id'),
            // castemos a un input
            "rating" => (int) request('rating_input'),
            "comment" => request('message')
        ]);
        return back()->with('message', ['success', __('Muchas gracias por valorar el curso')]);
    }

    public function create()
    {
        $course = new Course;
        $btnText = __("Enviar Curso para revision");
        return view('courses.form',compact('course','btnText'));
        
    }

    public function store (CourseRequest $course_request) {
/*        // si ha pasado la validacion se muestra en la pantalla con los datos obtenidos
            dd($course_request->all());*/

        // Aqui usamos los parametros que habias creado en nuestro helper
        // donde key = picture(nombre del archivo  ) y path (ruta del archivo)
        // subida del archivo uploadFile
        $picture = Helper::uploadFile('picture', 'courses');
        // dentro de la peticion $course_request exista una nueva varible 'picture'
        // con merge le pasamos un array donde va hacer el valor $picture
        $course_request->merge(['picture' => $picture]);
        // conseguimos el id del teacher que crea el curso
        $course_request->merge(['teacher_id' => auth()->user()->teacher->id]);
        // el curso esta en pendiento
        $course_request->merge(['status' => Course::PENDING]);
        // insertar en la tabla Course toda la infromacion del  $course_request
        Course::create($course_request->input());

        // luego enviamos un mensaje de exito de envio
        return back()->with('message', ['success', __('Curso enviado correctamente, recibirá un correo con cualquier información')]);


    }

    public function edit ($slug) {
        // Esta funcion para visualizar el editar y te cargue los datos en pantalla
        // juntamentente con las relaciones de requirements goals
        // y para tambien contar los requirements y goals
        // para luego utilizarlo requirements_count t goals_count
        $course = Course::with(['requirements', 'goals'])->withCount(['requirements', 'goals'])
            ->whereSlug($slug)->first();
        $btnText = __("Actualizar curso");
        return view('courses.form', compact('course', 'btnText'));
    }

    public function update (CourseRequest $course_request, Course $course) {

        // si deseas primero  le envias
        // dd($course); para ver los datos que se envian y darle click haber que se muestra en pantalla

        // si es que subimos un archivo va a tener que eliminar la imagen que que tenenia anteriormente
        // para luego subirla y darle la ruta
        // y con merge vamos a guardarla en un array
        if($course_request->hasFile('picture')) {
            \Storage::delete('courses/' . $course->picture);
            $picture = Helper::uploadFile( "picture", 'courses');
            $course_request->merge(['picture' => $picture]);
        }
        // con fill vamos a guardar todos los input a la BD
        $course->fill($course_request->input())->save();
        return back()->with('message', ['success', __('Curso actualizado')]);
    }
    public function destroy (Course $course) {
        try {
            $course->delete();
            return back()->with('message', ['success', __("Curso eliminado correctamente")]);
        } catch (\Exception $exception) {
            return back()->with('message', ['danger', __("Error eliminando el curso")]);
        }
    }
}
