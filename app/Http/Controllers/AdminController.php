<?php

namespace App\Http\Controllers;

use App\Course;
use App\Mail\CourseApproved;
use App\Mail\CourseRejected;

use Illuminate\Http\Request;

use App\VueTables\EloquentVueTables;

class AdminController extends Controller
{
    public function courses () {
        return view('admin.courses');
    }

    public function coursesJson () {
        // si es una peticion ajax
        if(request()->ajax()) {
            // creamos una instancia a partir de EloquentVueTables
            $vueTables = new EloquentVueTables;
            // y la data sera $vueTables de una instancia de curso
            //  get(modelo, campos,relaciones);
            $data = $vueTables->get(new Course, ['id', 'name', 'status'], ['reviews']);
            return response()->json($data);
        }
        return abort(401);
    }

    public function updateCourseStatus () {
        // solo para peticion ajax
        if (\request()->ajax()) {
            // cogemos el curso que vamos a encontrar

            // en el archivo Courses.vue
            // {courseId: row.id, status: newStatus},
            // aqui le pasamos el id ya que courseId tiene el id
            $course = Course::find(\request('courseId'));

            // casteamos el valor
            // si elcurso no esta aprovado  (int) $course->status !== Course::PUBLISHED
            // ! $course->previous_approved ademas tenemos un campo en la tabla si esta aprovado o no
            // ademas de eso queremos aprobar ese curso mediante un request
            if(
                (int) $course->status !== Course::PUBLISHED &&
                ! $course->previous_approved &&
                \request('status') === Course::PUBLISHED
            ) {
                // procedemos a aprobarlo
                $course->previous_approved = true;
                // le enviamos al profesor
                // y le enviamos todos los campos del curso  con CourseApproved que iniciarlmente
                // se creo para recibir ese parametro
//                public function __construct(Course $course)

                \Mail::to($course->teacher->user)->send(new CourseApproved($course));
            }

            if(

                // si el curso esta deiferente de rechazado
                // el estado en la tabla es
                (int) $course->status !== Course::REJECTED &&
                ! $course->previous_rejected &&
                // ademas de eso queremos rechazar ese curso mediante un request
                \request('status') === Course::REJECTED
            ) {
                $course->previous_rejected = true;
                \Mail::to($course->teacher->user)->send(new CourseRejected($course));
            }

            $course->status = \request('status');
            $course->save();
            return response()->json(['msg' => 'ok']);
        }
        return abort(401);
    }
    public function students () {
        return view('admin.students');
    }

    public function teachers () {
        return view('admin.teachers');
    }
}
