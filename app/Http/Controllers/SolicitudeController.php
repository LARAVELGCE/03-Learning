<?php

namespace App\Http\Controllers;

use App\Role;
use App\Teacher;
use Illuminate\Http\Request;

class SolicitudeController extends Controller
{
    public function teacher () {
        // Usuario autennticado
    	$user = auth()->user();
    	// si no es profesor
    	if ( ! $user->teacher) {
    		try {
    		    // crea una transacion
                //
			    \DB::beginTransaction();
			    // cambiale de rol al usuario para que sea profesor en la tabla Usuarios
			    $user->role_id = Role::TEACHER;
			    // Pero en la tabla teacher se Agregara  el usuario que le cambiastes de alumno a profesor
			    Teacher::create([
			    	'user_id' => $user->id
			    ]);
			    $success = true;
		    } catch (\Exception $exception) {
    			\DB::rollBack();
    			$success = $exception->getMessage();
		    }

		    if ($success === true) {
    			\DB::commit();
    			auth()->logout();
    			auth()->loginUsingId($user->id);
			    return back()->with('message', ['success', __("Felicidades, ya eres instructor en la plataforma")]);
		    }

		    return back()->with('message', ['danger', $success]);
	    }
	    return back()->with('message', ['danger', __("Algo ha fallado")]);
    }
}
