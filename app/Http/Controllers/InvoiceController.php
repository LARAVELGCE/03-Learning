<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class InvoiceController extends Controller
{
    public function admin () {
        //  Coleccion Vacia
    	$invoices = new Collection;
    	// si el usuario esta suscrito a un plan o tambien dentro
        // de la tabla user el stripe_id tiene un  valor que no sea null
    	if (auth()->user()->stripe_id) {
    	    // invoces()  => obtendra las facturas
    		$invoices = auth()->user()->invoices();
	    }
	    return view('invoices.admin', compact('invoices'));
    }
    public function download ($id) {
        // vamos a pasar el dowloadinvoce de stripe
        // y le pasamos el id
        // el vendor y el product
        return request()->user()->downloadInvoice($id, [
            "vendor" => "Mi empresa",
            "product" => __("Suscripción")
        ]);
    }

}
