<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rules\StrengthPassword;

class ProfileController extends Controller
{
    // Aqui cargara con los usuarios que se registraron con redes sociales
    // por eso lo load('dentro de l modelo user hay una modelo
    //que lo relaciona que es socialAccount ')
    public function index () {
        $user = auth()->user()->load('socialAccount');
        return view('profile.index', compact('user'));
    }

    public function update()
    {
        $this->validate(request(), [
            // aqui le indica que los campos de type password que hay en forumulario deben de coincidir
            // el primero que le paso es pa propiedad confirmed y el otro
            // las reglas manualmente que yo hize
            'password' => ['confirmed', new StrengthPassword]
        ]);
        // obtener el usuario
        $user = auth()->user();
        // obtener el password y lo guardamos encriptado
        $user->password = bcrypt(request('password'));
        $user->save();
        return back()->with('message', ['success', __("Usuario actualizado correctamente")]);
    }
}
