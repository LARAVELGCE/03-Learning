<?php

namespace App\Providers;
use App\Course;
use App\Policies\CoursePolicy;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // all  lo que hace aqui se  registra en la funcion 26 no te olvides
        // la referencia
        /*se App\Course;
        use App\Policies\CoursePolicy;*/
        Course::class => CoursePolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
