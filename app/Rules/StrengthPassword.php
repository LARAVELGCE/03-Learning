<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class StrengthPassword implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // si no viene valor retorna un true
        if ( ! $value) {
            return true;
        }
        // una mayuscula por lo minimo
        $uppercase          = preg_match('@[A-Z]@', $value);
        // una minuscula por lo minimo
        $lowercase          = preg_match('@[a-z]@', $value);
        // un numero por lo minimo
        $number             = preg_match('@[0-9]@', $value);
        // mas o igual de 8 numero
        $length             = strlen($value) >= 8;

        $success = true;

        // si no cumple ninguna validacion retorna false
        if ( ! $uppercase || ! $lowercase || ! $number || ! $length) {
            $success = false;
        }
        // si pasa retorna success (verdadero )

        return $success;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('El :attribute debe tener 8 caracteres, un número, una letra mayúscula y una letra minúscula');
    }
}
