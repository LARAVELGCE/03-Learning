<?php
namespace App\VueTables;

interface VueTablesInterface {
                       // ( la tabla que sea el modelo ,$fields => columnnas , las relaciones )
	public function get($model, Array $fields, Array $relations = []);
}