<?php

namespace App\Policies;

use App\User;
use App\Course;
use App\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class CoursePolicy
{
    use HandlesAuthorization;


    // Si el usuario es diferente de profesor
    // o el usuario id del teacher es discinto al teacher_id del curso
    public function opt_for_course (User $user, Course $course) {
        return ! $user->teacher || $user->teacher->id !== $course->teacher_id;
    }

    public function subscribe (User $user) {
        // si el rol es distinto a adminnistrador ya que este este tendra acceso a all
        // y tambien el usuario no este suscrito a un plan
        return $user->role_id !== Role::ADMIN && ! $user->subscribed('main');
    }

    public function inscribe (User $user, Course $course) {
        // si el usuario se puede inscribir al curso
        // Comprobar si dentro de la relacion de muchos a muchos
        // si el estudiante ya esta escrito en el curso
        return ! $course->students->contains($user->student->id);
    }

    public function review (User $user, Course $course) {
        // Si el estudiante no ha hecho una valoracion puede hacerlo
        return ! $course->reviews->contains('user_id', $user->id);
    }


}
