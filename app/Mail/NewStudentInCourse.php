<?php

namespace App\Mail;

use App\Course;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewStudentInCourse extends Mailable
{
    use Queueable, SerializesModels;

    // aqui declaramos las variables
    private $course;
    private $student_name;

    // añadimos el la clase curso y el nombre del estudiate
    public function __construct(Course $course, $student_name)
    {
        $this->course = $course;
        $this->student_name = $student_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            // El  motivo del mensaje
            ->subject(__("Nuevo estudiante inscrito en tu curso"))
            // le enviamos un markdown
            ->markdown('emails.new_student_in_course')
            // y con variables le enviaraemos el curso y el nombre del estudiante
            ->with('course', $this->course)
            ->with('student', $this->student_name);
    }
}
