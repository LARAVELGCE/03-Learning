<?php

namespace App\Mail;

use App\Course;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CourseApproved extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Course
     */
    //Inicializar el curso
    private $course;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Course $course)
    {
        //Inicializar el curso
        $this->course = $course;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject(__("Felicidades"))
            // ruta del archivo
            ->markdown('emails.course_approved')
//            enviamo el curso a la template
            ->with('course', $this->course);
    }
}
