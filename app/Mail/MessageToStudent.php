<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MessageToStudent extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var
     */
    // Iniciar el parametro 1
    private $teacher;
    /**
     * @var
     */
    // Iniciar el parametro 2
    private $text_message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    // Le pasaremos dos parametros
    public function __construct($teacher, $text_message)
    {
        //
        $this->teacher = $teacher;
        $this->text_message = $text_message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Lo mandamos con estas dos variables que declaramos arriba
        return $this->subject(__("Mensaje de :teacher ", ['teacher' => $this->teacher]))
                    ->markdown('emails.message_to_student')
                    ->with('text_message', $this->text_message);
    }
}
