<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Course
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $teacher_id
 * @property int $category_id
 * @property int $level_id
 * @property string $name
 * @property string $description
 * @property string $slug
 * @property string|null $picture
 * @property string $status
 * @property int $previous_approved
 * @property int $previous_rejected
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course wherePreviousApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course wherePreviousRejected($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereTeacherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereUpdatedAt($value)
 */
class Course extends Model
{

    protected $fillable = ['teacher_id', 'name', 'description', 'picture', 'level_id', 'category_id', 'status'];


    // como nuestra tabla course tiene softDeletes y al final de la tabla
    // se establecio un campo deleted_at la tenemos que establecer aqui
    // para el borrado logico
    // aqui aplicamos el softDeletes para el borrado logico
    use softDeletes;

    const PUBLISHED = 1;
    const PENDING = 2;
    const REJECTED = 3;

    // creamoa un evento boot para guardar y actualizar
    public static function boot () {
        parent::boot();
        // cuando se esta guardando o salvando
        // quiero que mi slug sea con la separacion de
        // caracteres de name
        static::saving(function(Course $course) {
            if( ! \App::runningInConsole() ) {
                $course->slug = str_slug($course->name, "-");
            }
        });
        // cuando se haya guardado
        static::saved(function (Course $course) {
            // hacemos esto para evitar cuando hacemos migraciones a BD
            // por eso diferente a la ejecucion en consola
            if ( ! \App::runningInConsole()) {
                // si existen requisitoss
                if ( request('requirements')) {
                    // hacemos un foreach para recorrer los requerimientos
                    // con su llave-$key y su valor-$requirement_input
                    foreach (request('requirements') as $key => $requirement_input) {
                        // si tenemos valor
                        if ($requirement_input) {
                            // comprueba si existe el id
                            // si existe actualizamos
                            // si no existe lo creamos
                            Requirement::updateOrCreate(['id' => request('requirement_id'. $key)], [
                                'course_id' => $course->id,
                                'requirement' => $requirement_input
                            ]);
                        }
                    }
                }
                 // de la misma manera con las metas
                if(request('goals')) {
                    foreach(request('goals') as $key => $goal_input) {
                        if( $goal_input) {
                            Goal::updateOrCreate(['id' => request('goal_id'.$key)], [
                                'course_id' => $course->id,
                                'goal' => $goal_input
                            ]);
                        }
                    }
                }
            }
        });
    }

    // Para mostrar las imagenes
    public function pathAttachment () {
        return "/images/courses/" . $this->picture;
    }
    // Cada curso pertenece a una categoria
    public function category () {
        return $this->belongsTo(Category::class)->select('id', 'name');
    }

    // Cada curso pertenece a una nivel
    public function level () {
        return $this->belongsTo(Level::class)->select('id', 'name');
    }

    // Cada curso pertenece a  muchos estudiante
    public function students () {
        return $this->belongsToMany(Student::class);
    }
    // Cada curso pertenece a una profesor
    public function teacher () {
        return $this->belongsTo(Teacher::class);
    }
    // Cada curso pertenece puede tener muchas metas con su id y forekey
    public function goals () {
        return $this->hasMany(Goal::class)->select('id', 'course_id', 'goal');
    }
    // Cada curso pertenece puede tener muchas valoraciones con su id y forekey y mas
    public function reviews () {
        return $this->hasMany(Review::class)->select('id', 'user_id', 'course_id', 'rating', 'comment', 'created_at');
    }
    // Cada curso pertenece puede tener muchas requirimientos con su id y forekey y mas
    public function requirements () {
        return $this->hasMany(Requirement::class)->select('id', 'course_id', 'requirement');
    }

    // con este atrivuto obtenemos el valor de rating getXXXXXXAttribute
    public function getCustomRatingAttribute () {
        return $this->reviews->avg('rating');
    }


    // Slug para redifirigir a cda curso por detalle
    public function getRouteKeyName() {
        return 'slug';
    }

    // Metodo de Eloquent para contar sus opiniones y los estudiantes del curso

    protected $withCount = ['reviews', 'students'];

    #attributes: array:16 [▼
        /*"id" => 1
        "teacher_id" => 1
        "category_id" => 1
        "level_id" => 3
        "name" => "Qui sit explicabo mollitia nemo quae."
        "description" => "Voluptas et autem illo qui odio. Rerum sit ex ea qui dicta. Qui sit cum enim inventore molestiae reiciendis."
        "slug" => "qui-sit-explicabo-mollitia-nemo-quae"
        "picture" => "1b6e6cdd8dd88f21beadb977fb03ead3.jpg"
        "status" => "1"
        "previous_approved" => 1
        "previous_rejected" => 0
        "created_at" => "2018-04-24 16:25:11"
        "updated_at" => "2018-04-24 16:25:11"
        "deleted_at" => null
        "reviews_count" => 1
        "students_count" => 0*/

        /*me aparecieron estos dos ultimo

        "reviews_count" => 1
        "students_count" => 0*/

        // Cursos que esten relacionado con la misma categoria
    public function relatedCourses () {
        return Course::with('reviews')->whereCategoryId($this->category->id)
            ->where('id', '!=', $this->id)
            ->latest()
            ->limit(6)
            ->get();
    }
}
