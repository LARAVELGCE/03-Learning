<?php
// ruta de nuestro directorio
namespace App\Helpers;

class Helper {
    // creamos un funcion donde le pasamos un key  y path(direccion)
    public static function uploadFile($key, $path) {
        request()->file($key)->store($path);
        // retornar el nombre del archivo qque ha guardado
        return request()->file($key)->hashName();
    }
}