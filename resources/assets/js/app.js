
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// usamos el serverTable de vue-tables-2 y utilizando vue.use
// donde le pasaremos el framework bootstrap4 y el tema default
// ServerTable = interactuar datos con el servidor y no hacen
// peticiones al servidor mas bien los datos
// se la pasan al componente directamente
import {ServerTable} from 'vue-tables-2';
Vue.use(ServerTable, {}, false, 'bootstrap4', 'default');

// Vue HTTP Resource para hacer peticiones tipo post por ejemplo
import VueResource from 'vue-resource'
Vue.use(VueResource);
// Vue HTTP Resource

// Importaremos aqui y recuerda ejecutar npm run dev para que guarde los cambios
import StripeForm from './components/StripeForm';
Vue.component('stripe-form', StripeForm);
// importamos el componente que creamos para usarlo
import Courses from './components/Courses';
Vue.component('courses-list', Courses);

const app = new Vue({
    el: '#app'
});
