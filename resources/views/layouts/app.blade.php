<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
    @stack('styles')
</head>
<body>

    @include('partials.navigation')
    @yield('jumbotron')
    <div id="app">
        {{--Justo aqui lo llamamos lo que creamos en el Vue --}}
        {{--<stripe-form></stripe-form>--}}
        <main class="py-4">
            {{--si existe la session message --}}
            @if(session('message'))
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        {{--Como ves aqui  session()->flash('message', ['danger', que
                        viene de login controller handleProviderCallback() --}}
                        <div class="alert alert-{{ session('message')[0] }}">
                            <h4 class="alert-heading">{{ __("Mensaje informativo") }}</h4>
                            {{--session()->flash('message', ['0', __("Inicio de sesión cancelado")]--}}
                            <p>{{ session('message')[1] }}</p>
                        </div>
                    </div>
                </div>
            @endif
            @yield('content')
        </main>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    @stack('scripts')
</body>
</html>
