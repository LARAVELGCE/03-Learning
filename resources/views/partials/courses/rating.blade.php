<div>
    <ul class="list-inline">
        {{--Aqui le pondre depende cuantas estrellas tiene si tiene mas de una estrella
         se le colocara la clase yellow sino no se le pone estrella--}}
        <li class="list-inline-item"><i class="fa fa-star{{ $rating >= 1 ? ' yellow' : '' }}"></i></li>
        <li class="list-inline-item"><i class="fa fa-star{{ $rating >= 2 ? ' yellow' : '' }}"></i></li>
        <li class="list-inline-item"><i class="fa fa-star{{ $rating >= 3 ? ' yellow' : '' }}"></i></li>
        <li class="list-inline-item"><i class="fa fa-star{{ $rating >= 4 ? ' yellow' : '' }}"></i></li>
        <li class="list-inline-item"><i class="fa fa-star{{ $rating >= 5 ? ' yellow' : '' }}"></i></li>
    </ul>
</div>