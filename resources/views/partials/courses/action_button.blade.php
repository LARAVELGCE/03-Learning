<div class="col-2">
    @auth
        @can('opt_for_course', $course)
          {{-- Si el usuario es diferente de profesor
             o el usuario id del teacher es discinto al teacher_id del curso--}}
             @can('subscribe', \App\Course::class)
              {{--   si el rol es distinto a adminnistrador ya que este este tendra acceso a all
                 y tambien el usuario no este suscrito a una suscripcion --}}
                <a class="btn btn-subscribe btn-bottom btn-block" href="{{route('subscriptions.plans')  }}">
                    <i class="fa fa-bolt"></i> {{ __("Subscribirme") }}
                </a>
             @else
                {{--  Si no se cumple  ---  si el rol es igua  a adminnistrador ya que este este tendra acceso a all
               y tambien el usuario no este suscrito a una suscripcion el usuario puede inscribirse al curso  --}}
                 @can('inscribe', $course)
                    <a class="btn btn-subscribe btn-bottom btn-block" href="{{ route('courses.inscribe', ['slug' => $course->slug]) }}">
                        <i class="fa fa-bolt"></i> {{ __("Inscribirme") }}
                    </a>
                 @else
                    <a class="btn btn-subscribe btn-bottom btn-block" href="#">
                        <i class="fa fa-bolt"></i> {{ __("Inscrito") }}
                    </a>
                 @endcan
             @endcan

        @else
            {{-- Si esto no se cumple eres autor --- Si el usuario es diferente de profesor
            o el usuario id del teacher es discinto al teacher_id del curso--}}
            <a class="btn btn-subscribe btn-bottom btn-block" href="#">
                <i class="fa fa-user"></i> {{ __("Soy autor") }}
            </a>

        @endcan

    @else
        <a class="btn btn-subscribe btn-bottom btn-block" href="{{ route('login') }}">
            <i class="fa fa-user"></i> {{ __("Acceder") }}
        </a>


    @endauth
</div>