<form action="{{ route('subscriptions.process_subscription') }}" method="POST">
    @csrf
    <input
        class="form-control"
        name="coupon"
        placeholder="{{ __("¿Tienes un cupón?") }}"
    />
    {{--Tipo de producto bien el mensual, trimestral o anual --}}
    <input type="hidden" name="type" value="{{ $product['type'] }}" />
    <hr />
    <stripe-form
            {{-- el archivo .en optenemos el stripe_key --}}
        stripe_key="{{ env('STRIPE_KEY') }}"
        name="{{ $product['name'] }}"
        amount="{{ $product['amount'] }}"
        description="{{ $product['description'] }}"
    ></stripe-form>
</form>