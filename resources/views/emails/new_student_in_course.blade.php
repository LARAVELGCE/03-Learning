{{--Aqui creamos el markdow y con #h1 empezaos el mensaje --}}
@component('mail::message')
# {{ __("Nuevo estudiante en tu curso!") }}

{{ __("El estudiante :student se ha inscrito en tu curso :course, FELICIDADES", ['student' => $student, 'course' => $course->name]) }}
<img class="img-responsive" src="{{ url('storage/courses/' . $course->picture) }}" alt="{{ $course->name }}">

{{--El boton del slug nos redigira al detalle del curso por lo hace desde slug --}}
{{--Por ejemplo 'color' => 'red' es cambiable ya que por defecto las propiedades estan en
resource/vendor/mail/html/button.blde--}}
@component('mail::button', ['url' => url('/courses/' . $course->slug), 'color' => 'red'])
    {{ __("Ir al curso") }}
@endcomponent
{{--Luego aqui a traves del archivo env el nombre de la aplicacion se pondra y se mandara  al markdown--}}
{{ __("Gracias") }},<br>
{{ config('app.name') }}

@endcomponent