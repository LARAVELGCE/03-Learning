@extends('layouts.app')

@section('jumbotron')
    @include('partials.jumbotron', ['title' => 'Configurar tu perfil', 'icon' => 'user-circle'])
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endpush

@section('content')
    <div class="pl-5 pr-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ __("Actualiza tus datos") }}
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('profile.update') }}" novalidate>
                            @csrf
                            @method('PUT')

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">
                                    {{ __("Correo electrónico") }}
                                </label>
                                <div class="col-md-6">
                                    <input
                                            id="email"
                                            type="email"
                                            readonly
                                            class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                            name="email"
                                            value="{{ old('email') ?: $user->email }}"
                                            required
                                            autofocus
                                    />

                                    @if($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label
                                        for="password"
                                        class="col-md-4 col-form-label text-md-right"
                                >
                                    {{ __("Contraseña") }}
                                </label>

                                <div class="col-md-6">
                                    <input
                                            id="password"
                                            type="password"
                                            class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                            name="password"
                                            required
                                    />

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label
                                        for="password-confirm"
                                        class="col-md-4 col-form-label text-md-right"
                                >
                                    {{ __("Confirma la contraseña") }}
                                </label>

                                <div class="col-md-6">
                                    <input
                                            id="password-confirm"
                                            type="password"
                                            class="form-control"
                                            name="password_confirmation"
                                            required
                                    />
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __("Actualizar datos") }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                {{-- si el estudiante no es profesor mostrar el formylador para poder hacer
                la gestion de convertirse en profesor --}}
                @if( ! $user->teacher)
                    <div class="card">
                        <div class="card-header">
                            {{ __("Convertirme en profesor de la plataforma") }}
                        </div>
                        <div class="card-body">
                            <form action="{{ route('solicitude.teacher') }}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-outline-primary btn-block">
                                    <i class="fa fa-graduation-cap"></i> {{ __("Solicitar") }}
                                </button>
                            </form>
                        </div>
                    </div>
                @else
                    <div class="card">
                        <div class="card-header">
                            {{ __("Administrar los cursos que imparto") }}
                        </div>
                        <div class="card-body">
                            <a href="{{ route('teacher.courses') }}" class="btn btn-secondary btn-block">
                                <i class="fa fa-leanpub"></i> {{ __("Administrar ahora") }}
                            </a>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            {{ __("Mis estudiantes") }}
                        </div>
                        <div class="card-body">
                            <table
                                    class="table table-striped table-bordered nowrap"
                                    cellspacing="0"
                                    id="students-table"
                            >
                                <thead>
                                <tr>
                                    <th>{{ __("ID") }}</th>
                                    <th>{{ __("Nombre") }}</th>
                                    <th>{{ __("Email") }}</th>
                                    <th>{{ __("Cursos") }}</th>
                                    <th>{{ __("Acciones") }}</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                @endif
                    {{--Ver con que red social se  identifico el usuario
                    si se autentifico con Socialize --}}
                @if($user->socialAccount)
                    <div class="card">
                        <div class="card-header">
                            {{ __("Acceso con Socialite") }}
                        </div>
                        <div class="card-body">
                            <button class="btn btn-outline-dark btn-block">
                                {{ __("Registrado con") }}: <i class="fa fa-{{ $user->socialAccount->provider }}"></i>
                                {{ $user->socialAccount->provider }}
                            </button>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>
    @include('partials.modal')
@endsection
@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        let dt;
        let modal = jQuery("#appModal");
        // cuando esta preparada la pagina
        jQuery(document).ready(function() {
            // que lo convierta en una tabla de datable
            // con el id de la tabla
            dt = jQuery("#students-table").DataTable({
                //paginacion de 10 en 10
                pageLength: 10,
                // intervalo de paginacion
                lengthMenu: [ 5, 10, 25, 50, 75, 100 ],
                // mensaje de informacion mientras carga
                processing: true,
                // si es false la informacion es de un solo golpe
                // tiene que ser true para que la informacion controlada
                // sea desde el servidor
                serverSide: true,
                // con ajax la ruta que queremos mostrar
                // y para eso en nuestro metodo students tiene que estar
                // creado en TeachercController
                ajax: '{{ route('teacher.students') }}',
                // cambiar el idioma
                language: {
                    url: "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                },
                columns: [
                    {data: 'user.id', visible: false},
                    {data: 'user.name'},
                    {data: 'user.email'},
                    {data: 'courses_formatted'},
                    {data: 'actions'}
                ]

            });

            // evento click apuntando al boton azul de la tabla
            // ya que la clase es .btnEmail que fue creado en archivo
            // views/students/datatables/actions.blade
            jQuery(document).on("click", '.btnEmail', function (e) {
                // cada que hago click no se redirija arribasi sino se mantenga
                e.preventDefault();
                // creamos una constante que sera igual al campo
                //  data-id=" {{$user['id']}} " que vendra del blade views/students/datatables/actions.blade
                const id = jQuery(this).data('id');
                // accedemos a modal que fue creado en la linea 163 y referenciado con
                // #appModal que vendra del archivo partials/modal.blade
                // de forma dinamica le añadimos en la clase .modal-title el texto eviar mensaje
                // quedara asi  <h5 class="modal-title" id="appModalLabel">Enviar mensaje</h5>
                modal.find('.modal-title').text('{{ __("Enviar mensaje") }}');
                // de la misma manera en el id del boton Enviar mensaje
                // <button type="button" class="btn btn-primary" id="modalAction">Enviar mensaje</button>
                // con show mostramos el modal
                modal.find('#modalAction').text('{{ __("Enviar mensaje") }}').show();
                // creamos un formulario
                let $form = $("<form id='studentMessage'></form>");
                // con append lo ponemos adentro del form y value="${id}
                // vendra del views/students/datatables/actions.blade
                // debe ser igual data-id="{{$user['id']}}"
                $form.append(`<input type="hidden" name="user_id" value="${id}" />`);
                $form.append(`<textarea class="form-control" name="message"></textarea>`);
                // dentro la clase .modal-body que esta partials/modal.blade
                // se le añade dentro la variable $form que contiene
                // todos lo especificado
                modal.find('.modal-body').html($form);
                // y con .modal lo lanzamos
                modal.modal();
            });

            // con el evento click al boton enviar mensaje capturamos el id
            jQuery(document).on("click", "#modalAction", function (e) {
                // usamos el metodo ajax para que no recargue
                jQuery.ajax({
                    // creamos la ruta
                    url: '{{ route('teacher.send_message_to_student') }}',
                    // usamos el metodo post para procesa
                    type: 'POST',
                    // usamos un header para que laravel entienda csrf-token
                    // que va ser obtenido de layouts/app.blade
                    //     <!-- CSRF Token -->
                    // <meta name="csrf-token" content="{{ csrf_token() }}">
                    headers: {
                        'x-csrf-token': $("meta[name=csrf-token]").attr('content')
                    },
                    // definir studentMessage ya que es el id de  nuestro formulario
                    // <form id='studentMessage'></form> que esta en linea 216
                    data: {
                        // convertimos con serialize para que pueda ser enviado via http
                        // al controlador utilizando la ruta teacher.send_message_to_student
                        // y lo devuelve en cadena
                        // ejemplo cuando escribo en el formulario ola ola
                        // info: user_id=66&message=ola%20ola
                        // esto lo podras ver en network pestaña de chrome F12
                        // NAME : end_message_to_student
                        // al final veras form-data
                            // info: user_id=66&message=ola%20ola
                        info: $("#studentMessage").serialize()
                    },
                    // creamos una variable para poder enviar una respuesta
                    // si la respuesra es verdadera
                    // oculta el boton enviar mensaje del modal
                    // y dentro del div  modal-body
                    // una alerta de mensaje correcto de lo contrario un alerta falsa
                    success: (res) => {
                        if(res.res) {
                            modal.find('#modalAction').hide();
                            modal.find('.modal-body').html('<div class="alert alert-success">{{ __("Mensaje enviado correctamente") }}</div>');
                        } else {
                            modal.find('.modal-body').html('<div class="alert alert-danger">{{ __("Ha ocurrido un error enviando el correo") }}</div>');
                        }
                    }
                })
            })


        })
    </script>
@endpush

