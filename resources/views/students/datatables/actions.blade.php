<a
        href="#"
       {{-- cuando pulsas se abre el modal --}}
        data-target="#appModal"
        title="{{ __("Enviar mensaje") }}"
        {{--El user id de cada columna a quien enviaremos el mensaje --}}
        data-id="{{$user['id']}}"
        class="btn btn-primary btnEmail"
>
    <i class="fa fa-envelope-square"></i>
</a>