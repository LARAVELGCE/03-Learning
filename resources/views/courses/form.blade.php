@extends('layouts.app')

@section('jumbotron')
    @include('partials.jumbotron', ['title' => 'Dar de alta un nuevo curso', 'icon' => 'edit'])
@endsection

@section('content')
    <div class="pl-5 pr-5">
        {{--En nuestro controlador CoursesController tenemos el metodo create()donde devolvemos
         una instancia pero en esta caso vemos que esta instancia no devuelve un id
          --}}
        <form
                method="POST"
                {{--Si no exite el id del curso se va procesar store (guardar una nuevo )
                si lo tiene solo va actualizar el id del regustro y ademas le paso el slug --}}
                        {{--Esto nos ayudara si es que va a actualizar o va crear uno nuevo--}}
                action="{{ ! $course->id ? route('courses.store') : route('courses.update', ['slug' => $course->slug])}}"
                {{--que no valide el navegador --}}
                novalidate
                {{--subida de archivos --}}
                enctype="multipart/form-data"
        >
            {{--si estamos en modo edicion usamos el metodo PUT--}}
            @if($course->id)
                @method('PUT')
            @endif

            @csrf

            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            {{ __("Información del curso") }}
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">
                                    {{ __("Nombre del curso") }}
                                </label>
                                <div class="col-md-6">
                                    <input
                                            name="name"
                                            id="name"
                                            class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                            value="{{ old('name') ?: $course->name }}"
                                            required
                                            autofocus
                                    />

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label
                                        for="level_id"
                                        class="col-md-4 col-form-label text-md-right"
                                >
                                    {{ __("Nivel del curso") }}
                                </label>
                                <div class="col-md-6">
                                    <select name="level_id" id="level_id" class="form-control">
                                        {{--Hacemos un foreach para recorrer  todos los level--}}
                                        {{--usamos el foreacj usando pluck que devolvera un array
                                        para seleccionar el nombre y como clave el id --}}

                                        {{--

                                            $catalog = array(
                                                "one" => 1,
                                                "two" => 2,
                                                "three" => 3,
                                                "four" => 4
                                            );

                                            foreach($catalog as $id => $item) {
                                            echo $id . " - " . $item . "<br>";
                                            }

                                                    one - 1
                                                    two - 2
                                                    three - 3

                                        --}}
                                       {{-- foreach($pluck('name', 'id')as $id => $name)--}}
                                        @foreach(\App\Level::pluck('name', 'id') as $id => $level)
                                            {{--si  el level_id que es mandado  traves de la vista
                                                es igual al  === $id que es enviado a traves del metodo post del formularip
                                                este es 'select'

                                                o sino el  $course->level_id que sera cuando cambias de level_id y es igual al
                                                 === $id que es enviado a traves del metodo post del formularip
                                                este es 'select'
                                            --}}
                                            <option {{ (int) old('level_id') === $id || $course->level_id === $id ? 'selected' : '' }}
                                                    value="{{ $id }}">
                                                {{--El nombre del nivel $level --}}
                                                  {{ $level }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="category_id" class="col-md-4 col-form-label text-md-right">{{ __("Categoría del curso") }}</label>
                                <div class="col-md-6">
                                    <select name="category_id" id="category_id" class="form-control">
                                        {{--le agrego un order by porque para evitar que se repitan las categorias
                                        si se cae por el groupBy entonces anda config/database y cambia  'strict' => false --}}
                                        @foreach(\App\Category::groupBy('name')->pluck('name', 'id') as $id => $category)
                                            <option {{ (int) old('category_id') === $id || $course->category_id === $id ? 'selected' : '' }} value="{{ $id }}">
                                                {{ $category }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group ml-3 mr-2">
                                <div class="col-md-6 offset-4">
                                    <input
                                            type="file"
                                            class="custom-file-input{{ $errors->has('picture') ? ' is-invalid' : ''}}"
                                            id="picture"
                                            name="picture"
                                    />
                                    <label
                                            class="custom-file-label" for="picture"
                                    >
                                        {{ __("Escoge una imagen para tu curso") }}
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label
                                        for="description"
                                        class="col-md-4 col-form-label text-md-right">
                                    {{ __("Descripción del curso") }}
                                </label>
                                <div class="col-md-6">
                                <textarea
                                        id="description"
                                        class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                        name="description"
                                        required
                                        rows="8"
                                >{{ old('description') ?: $course->description }}</textarea>

                                    @if ($errors->has('description'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __("Requisitos para tomar el curso") }}</div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label
                                        for="requirement1"
                                        class="col-md-4 col-form-label text-md-right"
                                >
                                    {{ __("Requerimiento 1") }}
                                </label>
                                <div class="col-md-6">
                                    <input
                                            id="requirement1"
                                            {{--Para poder obligar que el usuario ponga mas de 2 requerimientos del curso
                                            tenemos que hacer que se rellene 2 veces el campo
                                             En este caso manejaremos un campo que es array y por eso lo hacemos con .0
                                             requirements.0 y requirements[] (donde se alojara todos los requirements)
                                             nos servira para poder manejar

                                             si hay error le agregamos la clase is-invalid o sino nada--}}
                                            class="form-control{{ $errors->has('requirements.0') ? ' is-invalid' : '' }}"
                                            name="requirements[]"
                                            {{--si el numero de requerimientos es mayor a 0 se muestra el requermiento de orden 0
                                            de lo contrario vacio   --}}

                                            value="{{ old('requirements.0') ? old('requirements.0') : ($course->requirements_count > 0 ? $course->requirements[0]->requirement : '') }}"
                                    />
                                    @if ($errors->has('requirements.0'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('requirements.0') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                @if($course->requirements_count > 0)
                                    <input
                                            type="hidden"
                                            name="requirement_id0"
                                            value="{{ $course->requirements[0]->id }}"
                                    />
                                @endif
                            </div>

                            <div class="form-group row">
                                <label
                                        for="requirement2"
                                        class="col-sm-4 col-form-label text-md-right"
                                >
                                    {{ __("Requerimiento 2") }}
                                </label>
                                <div class="col-md-6">
                                    <input
                                            id="requirement2"
                                            class="form-control{{ $errors->has('requirements.1') ? ' is-invalid' : '' }}"
                                            name="requirements[]"
                                            {{--si el numero de requerimientos es mayor a 1 se muestra el requermiento de orden 1
                                           de lo contrario vacio   --}}
                                            value="{{ old('requirements.1') ? old('requirements.1') : ($course->requirements_count > 1 ? $course->requirements[1]->requirement : '') }}"
                                    />

                                    @if ($errors->has('requirements.1'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('requirements.1') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                @if($course->requirements_count > 1)
                                    <input
                                            type="hidden"
                                            name="requirement_id1"
                                            value="{{ $course->requirements[1]->id }}"
                                    />
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __("¿Qué conseguirá el estudiante al finalizar el curso?") }}</div>

                        <div class="card-body">
                            <div class="form-group row">
                                <label
                                        for="goal1"
                                        class="col-sm-4 col-form-label text-md-right"
                                >
                                    {{ __("Meta 1") }}
                                </label>
                                <div class="col-md-6">
                                    <input
                                            id="goal1"
                                            class="form-control{{ $errors->has('goals.0') ? ' is-invalid' : '' }}"
                                            name="goals[]"
                                            value="{{ old('goals.0') ? old('goals.0') : ($course->goals_count > 0 ? $course->goals[0]->goal : '') }}"
                                    />
                                    @if ($errors->has('goals.0'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('goals.0') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                @if($course->goals_count > 0)
                                    <input type="hidden" name="goal_id0" value="{{ $course->goals[0]->id }}" />
                                @endif

                            </div>

                            <div class="form-group row">
                                <label
                                        for="goal2"
                                        class="col-sm-4 col-form-label text-md-right"
                                >
                                    {{ __("Meta 2") }}
                                </label>
                                <div class="col-md-6">
                                    <input
                                            id="goal2"
                                            class="form-control{{ $errors->has('goals.1') ? ' is-invalid' : '' }}"
                                            name="goals[]"
                                            value="{{ old('goals.1') ? old('goals.1') : ($course->goals_count > 1 ? $course->goals[1]->goal : '') }}"
                                    />
                                    @if ($errors->has('goals.1'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('goals.1') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                @if($course->goals_count > 1)
                                    <input type="hidden" name="goal_id1" value="{{ $course->goals[1]->id }}" />
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row mb-0">
                                <div class="col-md-4 offset-5">
                                    <button type="submit" name="revision" class="btn btn-danger">
                                        {{ __($btnText) }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </form>
    </div>
@endsection